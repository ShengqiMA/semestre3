<?php
	if(isset($_POST['boutonValider'])){
		$idJardin = $_POST['idJardin'];
		$x = rand(0,100);
		$y = rand(0,100);
		$valNutritif = rand(0,500);
		$nbRangMax = $_POST['nbRangMax'];
		$nbRangMin = $_POST['nbRangMin'];
		$nbRang = rand($nbRangMin, $nbRangMax);
		
		$typeMise = array();
		$typeMise[1] = 'semis';
		$typeMise[2] = 'plant';
		$typeMise[3] = 'greffe';
		
		$pRangGood = $_POST['pRangGood'];
		$pRangBad = $_POST['pRangBad'];
		
		$nbRangGood = round($pRangGood * $nbRang /100,0);
		$nbRangBad = round($pRangBad * $nbRang /100,0);
		$nbVariete = $_POST['nbVariete'];
		
		$nbLigneVarieteNull = countVarieteNull($connexion);
		$nbLigneJardin = countInstancesJardin($connexion, $idJardin);
		
		
		

		if($nbLigneJardin < 1){
			$message = "il n'a pas de Jardin avec idJardin = ". $idJardin;
		}else if($nbRangMin < $nbRangMax){
			$message = "rang max < rang min";
		}else if($nbRangBad+$nbRangGood > $nbRang){
			$message = "somme de pourcentage de rang dépassé";
		}else if($nbLigneVarieteNull < $nbRangBad+$nbRangGood){
			$message = "pas assez de variété pour placer!";
		}else{
			
			$ret = insererParcelle($connexion, $x, $y, $nbRang, $valNutritif, $idJardin);
			if($ret != FALSE){
				$message = "réussir de insérer la Parcelle";
			}else{
				$message = "erreur quand insérer";
			}  
			
			for($i = 0;$i < $nbRangGood;$i++){
				$idR = countInstances($connexion, 'Rang') + 1;
				$coorR = $i+1;
				$ret = insererRang($connexion, $i+1, $typeMise[rand(1,3)]);
				if($ret != FALSE){
					$message .= " rang ".$i;
				}else{
					$message = "erreur quand insérer la rang";
				}
				for($j = 1;$j <= $nbVariete;$j++){
					$ret = placerVariete($connexion, $idR, $nbVariete);
					if($ret != FALSE){
						$message .= " variété ".$j;
					}else{
						$message = "erreur quand insérer la variété";
					}
				}
			}
			for($i = $nbRangGood;$i < $nbRangGood+$nbRangBad;$i++){
				$idR = countInstances($connexion, 'Rang') + 1;
				$coorR = $i+1;
				$ret = insererRang($connexion, $i+1, $typeMise[rand(1,3)]);
				if($ret != FALSE){
					$message .= " rang ".$i;
				}else{
					$message = "erreur quand insérer la rang";
				}
				for($j = 1;$j <= $nbVariete;$j++){
					$ret = placerVarieteIndesirable($connexion, $idR, $nbVariete);
					if($ret != FALSE){
						$message .= " variété ".$j;
					}else{
						$message = "erreur quand insérer la variété";
					}
				}
			}
			for($i = $nbRangGood+$nbRangBad;$i < $nbRang;$i++){
				$idR = countInstances($connexion, 'Rang') + 1;
				$coorR = $i+1;
				$ret = insererRang($connexion, $i+1, $typeMise[rand(1,3)]);
				if($ret != FALSE){
					$message .= " rang ".$i;
				}else{
					$message = "erreur quand insérer la rang";
				}
			}			
			$sql=afficherParcelle($connexion, $idJardin);
		}
	}
?>