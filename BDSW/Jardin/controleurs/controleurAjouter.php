<?php
	if(isset($_POST['boutonValider'])){
		$nomV = trim($_POST['nomV']);
		$codePre = $_POST['codePre'];
		$labelPre = $_POST['labelPre'];
		$annee = $_POST['anneeVariete'];
		$descSemis = $_POST['descSemis'];
		$descPlantation = $_POST['descPlantation'];
		$descEntretien = $_POST['descEntretien'];
		$descRecolt = $_POST['descRecolt'];
		$nbJourLevee = $_POST['nbJourLevee'];
		$commentaire = $_POST['commentaire'];
		$dateDebutM = $_POST['dateDebutM'];
		$dateFinM = $_POST['dateFinM'];
		$dateDebutR = $_POST['dateDebutR'];
		$dateFinR = $_POST['dateFinR'];
		
		$idPlante = $_POST['idPlante'];
		$nbligneVariete = countInstancesVariete($connexion, $nomV);
		$nblignePlante = countInstancesPlante($connexion, $idPlante);
		if($nbligneVariete > 0){
			$message = "impossible de ajouter :la ligne ".$nomV." déjà exist dans la base";
		}else if($dateDebutM > $dateFinM || $dateDebutR > $dateFinR){
			$message = "dateDebut ne peut pas grand que dateFin";
		}else if($nblignePlante < 1){
			$message = "il n'y pas de plante avec idPlante = ".$idPlante;
		}else{
			$ret = insererVariete($connexion, $nomV, $codePre, $labelPre, $annee, $descSemis, $descPlantation, $descEntretien, $descRecolt,$nbJourLevee, $commentaire, $dateDebutM, $dateFinM, $dateDebutR, $dateFinR);
			if($ret != FALSE){
				$message = "réussir de insérer la ligne ".$nomV." ";
			}else{
				$message = "erreur quand insérer";
			}  
		}
	}

?>