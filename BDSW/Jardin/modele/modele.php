<?php

// connexion à la BD, retourne un lien de connexion
function getConnexionBD() {
	$connexion = mysqli_connect(SERVEUR, UTILISATRICE, MOTDEPASSE, BDD);
	if (mysqli_connect_errno()) {
	    printf("Échec de la connexion : %s\n", mysqli_connect_error());
	    exit();
	}
	return $connexion;
}

// déconnexion de la BD
function deconnectBD($connexion) {
	mysqli_close($connexion);
}
// nombre d'instances d'une table $nomTable
function countInstances($connexion, $nomTable) {
	$requete = "SELECT COUNT(*) AS nb FROM $nomTable";
	$res = mysqli_query($connexion, $requete);
	if($res != FALSE) {
		$row = mysqli_fetch_assoc($res);
		return $row['nb'];
	}
	return -1;  // valeur négative si erreur de requête (ex, $nomTable contient une valeur qui n'est pas une table)
}

function countVarieteNull($connexion) {
	$requete = "SELECT COUNT(*) AS nb FROM Variete WHERE idR IS NULL";
	$res = mysqli_query($connexion, $requete);
	if($res != FALSE) {
		$row = mysqli_fetch_assoc($res);
		return $row['nb'];
	}
	return -1;  // valeur négative si erreur de requête (ex, $nomTable contient une valeur qui n'est pas une table)
}

// retourne les instances d'une table $nomTable
function getInstances($connexion, $nomTable) {
	$nomTable = mysqli_real_escape_string($connexion, $nomTable);
	if($nomTable == "Type"){
		$requete = "SELECT DISTINCT types,sousType FROM Plante";
	}else{
		$requete = "SELECT * FROM $nomTable";
	}
	$res = mysqli_query($connexion, $requete);
	$instances = mysqli_fetch_all($res, MYSQLI_ASSOC);
	return $instances;
}

// retourne les instances de tableau Variété avec $nomV
function countInstancesVariete($connexion,$nomV){
	$nomV = mysqli_real_escape_string($connexion, $nomV);
	$requete = "SELECT COUNT(nomV) AS nb FROM Variete WHERE nomV = '".$nomV."'";
	$res = mysqli_query($connexion, $requete);
	if($res != FALSE) {
		$row = mysqli_fetch_assoc($res);
		return $row['nb'];
	}
	return -1;
}
// retourne les instances de tableau Plante avec $idPlante
function countInstancesPlante($connexion,$idPlante){
	$idPlante = mysqli_real_escape_string($connexion, $idPlante);
	$requete = "SELECT COUNT(idPlante) AS nb FROM Variete WHERE idPlante = '".$idPlante."'";
	$res = mysqli_query($connexion, $requete);
	if($res != FALSE) {
		$row = mysqli_fetch_assoc($res);
		return $row['nb'];
	}
	return -1;
}
// retourne les instances de tableau Jardin avec $idPlante
function countInstancesJardin($connexion,$idJardin){
	$idJardin = mysqli_real_escape_string($connexion, $idJardin);
	$requete = "SELECT COUNT(idJardin) AS nb FROM Jardin WHERE idJardin = '".$idJardin."'";
	$res = mysqli_query($connexion, $requete);
	if($res != FALSE) {
		$row = mysqli_fetch_assoc($res);
		return $row['nb'];
	}
	return -1;
}
// insérer un variété avec les attributs
function insererVariete($connexion, $nomV, $codePre, $labelPre, $annee, $descSemis, $descPlantation, $descEntretien, $descRecolt,$nbJourLevee, $commentaire, $dateDebutM, $dateFinM, $dateDebutR, $dateFinR){
		$nomV = mysqli_real_escape_string($connexion, $nomV);
		$codePre = mysqli_real_escape_string($connexion, $codePre);
		$labelPre = mysqli_real_escape_string($connexion, $labelPre);
		$annee = mysqli_real_escape_string($connexion, $annee);
		$descSemis = mysqli_real_escape_string($connexion, $descSemis);
		$descPlantation = mysqli_real_escape_string($connexion, $descPlantation);
		$descEntretien = mysqli_real_escape_string($connexion, $descEntretien);
		$descRecolt = mysqli_real_escape_string($connexion, $descRecolt);
		$nbJourLevee = mysqli_real_escape_string($connexion, $nbJourLevee);
		$commentaire = mysqli_real_escape_string($connexion, $commentaire);
		$dateDebutM = mysqli_real_escape_string($connexion, $dateDebutM);
		$dateFinM = mysqli_real_escape_string($connexion, $dateFinM);
		$dateDebutR = mysqli_real_escape_string($connexion, $dateDebutR);
		$dateFinR = mysqli_real_escape_string($connexion, $dateFinR);
		//$requete = "INSERT INTO Variete(nomV, codePre, labelPre, anneeVariete, descSemis, descPlantation, descEntretien, descRecolt, commentaireV) VALUES('".$nomV."', '". $codePre."', '". $labelPre."', '". $annee."', '".$descSemis."','".$descPlantation."','".$descEntretien."', '". $descRecolt."', '".$commentaire."')";
		$requete = "INSERT INTO Variete(nomV, codePre, labelPre, anneeVariete, descSemis, descPlantation, descEntretien, descRecolt, nbJourLevee, commentaireV, dateDebutM, dateFinM, dateDebutR, dateFinR) VALUES('".$nomV."', '". $codePre."', '". $labelPre."', '". $annee."', '".$descSemis."','".$descPlantation."','".$descEntretien."', '". $descRecolt."', '". $nbJourLevee."', '".$commentaire."', '".$dateDebutM."','".$dateFinM."', '".$dateDebutR."','".$dateFinR."')";
		//$requete = "INSERT INTO Variete(nomV, codePre, labelPre, anneeVariete, descSemis, descPlantation, descEntretien, descRecolt, commentaireV, dateDebutM, dateFinM) VALUES('".$nomV."', '". $codePre."', '". $labelPre."', '". $annee."', '".$descSemis."','".$descPlantation."','".$descEntretien."', '". $descRecolt."', '".$commentaire."', to_date('".$dateDebutM."','YYYY-MM-DD'), to_date('".$dateFinM."','YYYY-MM-DD'))";
		$res = mysqli_query($connexion, $requete);
		return $res;
}
// insérer un parcelle avec les attributs
function insererParcelle($connexion, $x, $y, $nbRang, $valNutritif, $idJardin){
	$requete = "INSERT INTO Parcelle(x, y, nbRang, valNutritif, idJardin) VALUES(".$x.",".$y.",".$nbRang.",".$valNutritif.",".$idJardin.")";
	$res = mysqli_query($connexion, $requete);
	return $res;
}
// insérer un rang avec les attributs
function insererRang($connexion, $coorR, $typeMise){
	$requete = "SELECT MAX(idParcelle) AS idMax FROM Parcelle";
	$res = mysqli_query($connexion, $requete);
	if($res != FALSE){
		$row = mysqli_fetch_assoc($res);
		$idParcelle = $row['idMax'];
	}
	$requete = "INSERT INTO Rang(coorR, typeMise, idParcelle) VALUES(".$coorR.",'".$typeMise."',".$idParcelle.")";
	$res = mysqli_query($connexion, $requete);
	return $res;
}
// modifié des idR de variété (placer des variété dans des rangs)
function placerVariete($connexion, $idR, $nbVariete){
	$requete = "UPDATE Variete SET idR = ".$idR." WHERE idR IS NULL ORDER BY rand() LIMIT ".$nbVariete;
	$res = mysqli_query($connexion, $requete);
	return $res;
}
// modifié des idR de variété de plante indésirable(placer des variété de plante indésirable dans des rangs)
function placerVarieteIndesirable($connexion, $idR, $nbVariete){
	$requete = "UPDATE Variete SET idR = ".$idR." WHERE idR IS NULL AND idV IN (SELECT idV FROM Variete NATURAL JOIN Plante WHERE categorie = 'indesirable') ORDER BY rand() LIMIT ".$nbVariete;
	$res = mysqli_query($connexion, $requete);
	return $res;
}
//afficher un parcelle avec idJardin, idParcelle,idR,idV,nomV et les lignes NULL
function afficherParcelle($connexion, $idJardin){
	$requete = "select idJardin, idParcelle, Rang.idR, idV, nomV FROM Jardin NATURAL JOIN Parcelle NATURAL JOIN Rang LEFT JOIN Variete on Variete.idR = Rang.idR WHERE idJardin = ".$idJardin." AND idParcelle = (select max(idParcelle) FROM Parcelle);";
	$res=mysqli_query($connexion, $requete);
	return $res;
}

function top5parcelles($connexion){
	$requete = "select idParcelle,COUNT(idParcelle) as nb FROM Rang GROUP BY idParcelle ORDER BY nb DESC LIMIT 5";
	$res=mysqli_query($connexion, $requete);
	return $res;
}
?>

