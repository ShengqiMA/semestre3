DROP TABLE IF EXISTS Couvrir;
DROP TABLE IF EXISTS Ornement;
DROP TABLE IF EXISTS Verger;
DROP TABLE IF EXISTS Potager;
DROP TABLE IF EXISTS Concequence;
DROP TABLE IF EXISTS Decoupe;
DROP TABLE IF EXISTS Produit;
DROP TABLE IF EXISTS PlusAdapter;
DROP TABLE IF EXISTS Semencier;
DROP TABLE IF EXISTS Sol;
DROP TABLE IF EXISTS Recolte;
DROP TABLE IF EXISTS Menace;
DROP TABLE IF EXISTS Variete;
DROP TABLE IF EXISTS Plante;
DROP TABLE IF EXISTS Rang;
DROP TABLE IF EXISTS Parcelle;
DROP TABLE IF EXISTS Jardin;


create table Jardin (
    idJardin int PRIMARY KEY AUTO_INCREMENT,
    nomJardin varchar(30),
    surfaceJardin int
);


create table Parcelle (
    idParcelle int PRIMARY KEY AUTO_INCREMENT,
    x int,
    y int,
    nbRang  int,
    valNutritif int,
    idJardin int,
    FOREIGN KEY (idJardin) REFERENCES Jardin(idJardin)
);


create table Rang(
    idR int PRIMARY KEY AUTO_INCREMENT,
    coorR int,
    typeMise varchar(30),
    idParcelle int,
    FOREIGN KEY(idParcelle) REFERENCES Parcelle(idParcelle)
);

create table Plante (
    idPlante int PRIMARY KEY AUTO_INCREMENT,
    nomPlante Varchar(50),
    nomLatin Varchar(50),
    categorie Varchar(30),
    types Varchar(50),
    sousType Varchar(50)
);

CREATE TABLE Menace(
	idMenace int,
	nomMenace varchar(30),
	descMenace varchar(50),
	solMenace varchar(50),
	idPlante int,
	
	PRIMARY KEY(idMenace),
	FOREIGN KEY(idPlante) REFERENCES Plante(idPlante)	
);

CREATE TABLE Variete(
	idV int PRIMARY KEY AUTO_INCREMENT,
	nomV varchar(30),
	codePre varchar(10),
	labelPre varchar(30),
	anneeVariete YEAR,
	descSemis varchar(50),
	descPlantation varchar(50),
	descEntretien varchar(50),
	descRecolt varchar(50),
	nbJourLevee int,
	dateDebutM date,
	dateFinM date,
	dateDebutR date,
	dateFinR date,
	commentaireV varchar(50),
	idPlante int,
	idR int,
	FOREIGN KEY (idR) REFERENCES Rang(idR),
	FOREIGN KEY (idPlante) REFERENCES Plante(idPlante)
);

CREATE TABLE Recolte(
	idRec int,
	qualiteRec varchar(30),
	quantiteRec int,
	commentaireR varchar(50),
	idV int,
	PRIMARY KEY(idRec),
	FOREIGN KEY(idV) REFERENCES Variete(idV)
);

CREATE TABLE Sol(
	idSol int,
	PRIMARY KEY(idSol)
);

CREATE TABLE Semencier(
	idSm int,
	nomSmencier varchar(50),
	siteSmencier varchar(50),
	PRIMARY KEY(idSm)
);

CREATE TABLE PlusAdapter(
	idV int,
	idSol int,
	adapter varchar(30),
	PRIMARY KEY(idV, idSol),
	FOREIGN KEY(idV) REFERENCES Variete(idV),
	FOREIGN KEY(idSol) REFERENCES Sol(idSol)
);


CREATE TABLE Produit(
	idV int,
	idSol int,
	version varchar(20),
	PRIMARY KEY(idV, idSol),
	FOREIGN KEY(idV) REFERENCES Variete(idV),
	FOREIGN KEY(idSol) REFERENCES Sol(idSol)
);
CREATE TABLE Concequence (
	idPlante1 int,
	idPlante2 int,
	PRIMARY KEY(idPlante1, idPlante2),
	FOREIGN KEY(idPlante1) REFERENCES Plante(idPlante),
	FOREIGN KEY(idPlante2) REFERENCES Plante(idPlante)
);

CREATE TABLE Potager (
	idJardin int,
	idSol int,
	PRIMARY KEY(idJardin, idSol),
	FOREIGN KEY(idJardin) REFERENCES Jardin(idJardin),
	FOREIGN KEY(idSol) REFERENCES Sol(idSol)
);

CREATE TABLE Verger (
	idJardin int,
	hMax int,
	PRIMARY KEY(idJardin),
	FOREIGN KEY(idJardin) REFERENCES Jardin(idJardin)
);

CREATE TABLE Ornement (
	idJardin int,
	PRIMARY KEY(idJardin),
	FOREIGN KEY(idJardin) REFERENCES Jardin(idJardin)
);

CREATE TABLE Couvrir(
	idPlante int,
	idR int,
	PRIMARY KEY(idPlante, idR),
	FOREIGN KEY(idPlante) REFERENCES Plante(idPlante),
	FOREIGN KEY(idR) REFERENCES Rang(idR)
);
INSERT INTO Variete(nomV, commentaireV, anneeVariete, codePre, labelPre) SELECT codeVariété, commentaire, annéeEnregistrement, codePrécocité, labelPrécocité FROM dataset.DonneesFournies ORDER BY codeVariété;
INSERT INTO Plante(nomPlante, nomLatin, types, sousType) SELECT DISTINCT nomEspèce, nomEspèceLatin, type, sousType FROM dataset.DonneesFournies GROUP BY nomEspèce ORDER BY nomEspèce;

UPDATE Variete
inner join dataset.DonneesFournies
on Variete.nomV = dataset.DonneesFournies.codeVariété
inner join Plante
on dataset.DonneesFournies.nomEspèce = Plante.nomPlante
set Variete.idPlante = Plante.idPlante;

INSERT INTO Jardin Values(1,'PremierJardin', 100);

UPDATE Plante SET categorie = 'indesirable' ORDER BY rand() LIMIT 50;