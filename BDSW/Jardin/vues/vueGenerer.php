
<form method="post" action="#" class = "joliforme">	
	<h1>
		Générer et afficher une parcelle 
	</h1>

	<label for="idIdJardin">
		<span>idJardin</span>
		<input type="number" value = "1" max = "10" min = "1" name="idJardin" id = "idIdJardin" required/>
	</label>

	<br>
	
	<label for="idNbRangMax">
		<span>Rang min</span> 
		<input type="number" value = "10" min = "0" name="nbRangMax" id = "idNbRangMax" required/ >
	</label>
	<br>
	
	<label for="idNbRangMin">
		<span>Rang max </span> 
		<input type="number" value = "10" min = "0" name="nbRangMin" id = "idNbRangMin"  required/>
	</label>
	<br>	
	
	<label for="idNbVariete">
		<span>nbVariété dans chaque rang </span>
		<input type="number" value = "1" max = "10" min = "1" name="nbVariete" id = "idNbVariete" required/>
	</label>
	<br>
	
	<label for="idPRangGood">
		<span>Rangs cultures</span> 
		<input type="number" value = "10" max = "100" min = "0" name="pRangGood" id = "idPRangGood"  required/>
		%
	</label>
	<br>	
	
	<label for="idPRangBad">
		<span>Rangs indésirables </span>
		<input type="number" value = "10" max = "100" min = "0" name="pRangBad" id = "idPRangBad"  required/>
		%
	</label>
	<br>	
	
	<input type="submit" name="boutonValider" value="Générer"/>
	
</form>
<article>
	<?php if(isset($message)) { ?>
		<p style="background-color: #f97f51;"><?= $message ?></p>
	<?php } ?>
	
	<div id = "tableau">

	<table>
		<tbody>
	<?php
	if(isset($sql)){
		while($row=mysqli_fetch_assoc($sql)){?>
		<tr class="tbTitle">
			<th>idJardin</th>
			<th>idParcelle</th>
			<th>idRang</th>
			<th>nom Variété</th>
			<th>idVariété</th>
		</tr>
		<tr class="tbContext">
			<td><?php echo $row['idJardin'];?></td>
			<td><?php echo $row['idParcelle'];?></td>
			<td><?php echo $row['idR'];?></td>
			<td><?php echo $row['nomV'];?></td>
			<td><?php echo $row['idV'];?></td>
			</tr>
	</div>

	<?php
		}
	}
	?>
	</tbody></table>
	

</article>