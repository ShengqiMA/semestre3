<form method = "post" action = "#" class = "joliforme">

	<h1>
		 Afficher les variétés
	</h1>
	
	<label for = "idChamp">Afficher la base de </label>
	<select name="champRech" id="idChamp">
		<option value="Plante">Plante</option>
		<option value="Variete">Variété</option>
		<option value="Type">Type</option>
	</select>
	<input type="submit" name="boutonValider" value="Afficher"/>
</form>

<article>
	<?php if(isset($message)) { ?>
		<p style="background-color: yellow;"><?= $message ?></p>
	<?php } ?>
	
	<?php if(isset($resultats)) { ?>	
		<ul>
		<?php 
			foreach($resultats as $instance) {  // nombre d'attributs variable dans les résultats (selon la table)
				echo '<li>';
				foreach($instance as $valeur)  // affichage de chaque valeur (correspondant à chaque attribut)
					echo $valeur . ' ';
				echo '</li>';
			}
		?>
		</ul>
	<?php } ?>
</article>
