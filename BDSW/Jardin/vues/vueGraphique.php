<form method="post" action="#" class = "joliforme">	
	<h1>
		Graphique
	</h1>

	<label for="idIdJardin">
		<span>idJardin :</span>
		<input type="number" value = "1" max = "10" min = "1" name="idJardin" id = "idIdJardin" required/>
	</label>

	<br>
	
	<label for="idNbRang">
		<span>Rang :</span> 
		<input type="number" value = "10" min = "0" name="nbRang" id = "idNbRang" required/ >
	</label>
	<br>
	

	<label for="idNbVariete">
		<span>nbVariété dans chaque rang :</span>
		<input type="number" value = "1" max = "10" min = "1" name="nbVariete" id = "idNbVariete" required/>
	</label>
	<br>
	
	<label for="idPRangGood">
		<span>Rangs cultures :</span> 
		<input type="number" value = "10" max = "100" min = "0" name="pRangGood" id = "idPRangGood"  required/>
		%
	</label>
	<br>	
	
	<label for="idPRangBad">
		<span>Rangs indésirables :</span>
		<input type="number" value = "10" max = "100" min = "0" name="pRangBad" id = "idPRangBad"  required/>
		%
	</label>
	<br>	
	
	<input type="submit" name="boutonValider" value="Générer"/>
	
</form>

<table class="graphique">
<?php
	if(isset($nbVariete) && isset($nbRang)){
		for($i = 1; $i <= $nbRangGood;$i++){
?>
			<tr class = "rang">
				<?php
					for($j = 1;$j <= $nbVariete;$j++){
						?>
						<td> <img src = "<?php echo "img/".$nomImage[rand(1,7)].".png"?>" alt = ""/></td>
						<?php
					}
				?>
			</tr>
<?php
		}
		for($i = $nbRangGood + 1;$i <= $nbRangGood+$nbRangBad;$i++){
			?>
			<tr class = "rang">
				<?php
					for($j = 1;$j <= $nbVariete;$j++){
						?>
						<td> <img src = "img/weeds.png" alt = ""/></td>
						<?php
					}
				?>
			</tr>
			<?php
		}
		for($i = $nbRangGood +$nbRangBad + 1;$i <= $nbRang;$i++){
			?>
			<tr class = "rang">
				<?php
					for($j = 1;$j <= $nbVariete;$j++){
						?>
						<td> <img src = "img/empty.png" alt = ""/></td>
						<?php
					}
				?>
			</tr>
			<?php
		}
	}
?>
</table>