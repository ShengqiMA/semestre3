
<form method="post" action="#" class="joliforme">	

	<h1>
		 Ajouter une variété
	</h1>
	<br>
	<label for="idNomV">nom Variété </label>
	<input type="text" value = "Abbys" name="nomV" id = "idNomV" required/>
	<br>
	<label for="idCodePre">codePre</label>
	<input type="text" value = "A1" name="codePre" id = "idCodePre" required/>
	<br>
	<label for="idLabelPre">labelPre</label>
	<input type="text" value = "demi-précoce" name="labelPre" id = "idLabelPre" required/>
	<br>
	<label for="idAnneeVariete">année Variété </label>
	<input type="number" value = "2021" max = "2021" min = "1990" name="anneeVariete" id = "idAnneeVariete"  >
	<br>
	<label for="idDescSemis">descSemis</label>
	<input type="text" value = "non descp" name="descSemis" id = "idDescSemis" required/>
	<br>
	<label for="idDescPlantation">descPlantation</label>
	<input type="text" value = "non plantation" name="descPlantation" id = "idDescPlantation" required/>
	<br>
	<label for="idDescEntretien">descEntretien</label>
	<input type="text" value = "non entretien" name="descEntretien" id = "idDescEntretien" required/>
	<br>
	<label for="idDescRecolt">descRecolt</label>
	<input type="text" value = "non descp" name="descRecolt" id = "idDescRecolt" required/>
	<br>
	<label for="idNbJourLevee">nbJourLevee </label>
	<input type="number" value = "1" max = "365" min = "0" name="nbJourLevee" id = "idNbJourLevee"  required/>
	<br>
	<label for="idCommentaire">commentaire </label>
	<input type="text" value = "non commentaire" name="commentaire" id = "idCommentaire" required/>
	<br>
	
	<label for="idDateDebutM">dateDebutM</label>
	<input type="date" value="2021-12-01" max = "<?= date("Y-m-d")?>" name="dateDebutM" id = "idDateDebutM" required/>
	<br>
	<label for="idDateFinM">dateFinM</label>
	<input type="date" value="2021-12-01" max = "<?= date("Y-m-d")?>" name="dateFinM" id = "idDateFinM" required/>
	<br>
	
	<label for="idDateDebutR">dateDebutR</label>
	<input type="date" value="2021-12-01" max = "<?= date("Y-m-d")?>" name="dateDebutR" id = "idDateDebutR" required/>
	<br>
	<label for="idDateFinR">dateFinR</label>
	<input type="date" value="2021-12-01" max = "<?= date("Y-m-d")?>" name="dateFinR" id = "idDateFinR" required/>
	<br>
	
	<label for="idIdPlante">idPlante </label>
	<input type="number" value = "1" min = "1" name="idPlante" id = "idIdPlante"  required/>
	<br>
	
	<input type="submit" name="boutonValider" value="Insérer"/>
</form>

<article>
<?php
    //echo " <p>Aujourd'hui：" . date("Y/m/d") . "</p>";
?>
	<?php if(isset($message)) { ?>
		<p style="background-color: #f97f51; color:white;"><?= $message ?></p>
	<?php } ?>
</article>