#include<iostream>
using namespace std;


void procedureAvecPointeur(int * ptr){
	cout << "var = " << ptr << endl;
	cout << "adr = " << &ptr << endl;
}
void procedureReference(int & rf){
	cout << "var = " << rf << endl;
	cout << "adr = " << &rf << endl;
}

//le paramètre val est une nouvelle valeur
void procedureAvecInt(int val){
	cout << "var = " << val << endl;
	cout << "adr = " << &val << endl;
}
int main(){
	int x = 1;
	int & rx = x;
	int * px = &x;
	cout << "x: " << x << " rx: " << rx << " px : " << (long int)px << endl;
	
	cout << (long int)&x << endl;
	cout << (long int)&rx << endl;
	cout << (long int)&px << endl;

	procedureAvecPointeur(px);
	procedureReference(rx);
	procedureAvecInt(x);
}
