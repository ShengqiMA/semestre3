#include<iostream>
#include "Liste.h"
#include "ElementL.h"
using namespace std;
Liste::Liste(){
	prem = NULL;
	last = NULL;
}

Liste::~Liste(){
	Cellule * actu = prem;
	Cellule * next;
	while(actu != NULL){
		next = actu->suivant;
		delete actu;
		actu = next; 
	}
	prem = NULL;
	last = NULL;
	cout << "Liste ditribuer" << endl;
}

List & List::operator = (const List & l){
	vider();
	Cellule prem = l.prem;
	Cellule last = l.last;
}

void Liste::vider(){
	Cellule * actu = prem;
	Cellule * next;
	while(actu != NULL){
		next = actu->suivant;
		delete actu;
		actu = next; 
	}
	prem = NULL;
	last = NULL;
}

bool Liste::estVide() const{
	if(prem == NULL && last == NULL) return true;
	else return false;
}

unsigned int Liste::nbElements() const{
	unsigned int nb = 0;
	Cellule * actu = prem;
	while(actu != NULL){
		nb++;
		actu = actu->suivant;
	}
	return nb;
}

ElementL Liste::iemeElement(unsigned int indice) const{
	if(!estVide() && indice < nbElements() && indice >= 0){
		Cellule * actu = prem;
		for(unsigned int i = 0;i < indice;i++){
			actu = actu->suivant;
		}
		return actu->info;
	}
	cout << "impossible de trouver valeaur d'indice" << endl;
	exit(EXIT_FAILURE);	
}

void Liste::modifierIemeElement(unsigned int indice, ElementL e){
	if(!estVide() && indice < nbElements() && indice >= 0){
		Cellule * actu = prem;
		for(unsigned int i = 0;i < indice;i++){
			actu = actu->suivant;
		}
		actu->info = e;
	}
}
void Liste::afficherGaucheDroite() const{
	Cellule * actu = prem;
	while(actu != NULL){
		afficherElementL(actu->info);
		cout << " ";
		actu = actu->suivant;
	}	
}

void Liste::afficherDroiteGauche() const{
	Cellule * actu = last;
	while(actu != NULL){
		afficherElementL(actu->info);
		cout << " ";
		actu = actu->precedent;
	}
}

void Liste::ajouterEnTete(ElementL e){
	Cellule * nouveau = new Cellule;
	nouveau->info = e;
	nouveau->suivant = prem;
	nouveau->precedent = NULL;
	if(prem != NULL){
		prem->precedent = nouveau;
	}else{
		last = nouveau;
	}
	prem = nouveau;
}

void Liste::ajouterEnQueue(ElementL e){
	Cellule * nouveau = new Cellule;
	nouveau->info = e;
	nouveau->suivant = NULL;
	nouveau->precedent = last;
	if(last != NULL){
		last->suivant = nouveau;	
	}else{
		prem = nouveau;
	}
	
	last = nouveau;
}

void Liste::supprimerTete(){
	if(!estVide()){
		Cellule * actu = prem->suivant;
		prem = actu;
		prem->precedent = NULL;
	}else{
		cout << "impossible de supprimer en raison de vide" << endl;
	}
}


