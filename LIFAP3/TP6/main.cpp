#include "Liste.h"
#include <iostream>
using namespace std;
int main(){
//a.
	Liste l;
	l.ajouterEnTete(3);
	l.ajouterEnTete(5);
	l.ajouterEnTete(4);
	l.ajouterEnQueue(1);
	l.afficherGaucheDroite();
	l.afficherDroiteGauche();
//b.
	if(!l.estVide()) cout << "YES" << endl;
	Liste l1;
	if(l1.estVide()) cout << "YES" << endl;
	
	
//e.

	l.supprimerTete();
	l.afficherGaucheDroite();
//f.
	l.vider();
	if(l.estVide()) cout << "YES" << endl;

//h.
	cout << l.nbElements() << endl;
	l.ajouterEnQueue(7);
	l.ajouterEnQueue(8);
	l.ajouterEnQueue(9);
	cout << l.nbElements() << endl;
//i.
	cout << "indice 0: " << l.iemeElement(0) << "  indice 2: " << l.iemeElement(2) << endl;
	
//j.
	l.modifierIemeElement(0, 10);
	l.afficherGaucheDroite();
	
}
