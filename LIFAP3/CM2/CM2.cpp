#include<iostream>
using namespace std;

int main(){
	
	//allocation dinamique de memoire
	//NEW ET DELETE
	//动态内存空间在TAS(堆)中分配 
	//普通变量在PILE(栈)中分配 
	int *p = new int(7);
	//分配空间后用NULL判断该空间是否分配成功 
	if (p==NULL){
		cout << "空间分配失败" << endl;
		//return ERROR;
	}
	cout << *p << endl;
	delete p;//释放空间 
	p = NULL;//删除指针p后一定要将p指向空 否则会产生危险 1.指向违背分配的空间 输出为随机数 2. 与其他指针指向同一空间  
	//---------------------------------------
	int *array = new int[10];
	array[0] = 1;
	array[9] = 10;
	cout << array[0] << '\n' << array[9] << endl;
	delete[] array;
	array = NULL;
	//---------------------------------------
	int **array2D = new int *[10];
	for(int i = 0;i < 10;i++)
	array2D[i] = new int[5];
	delete[] array2D;
	array2D = NULL;
	
	int tab[10];
	tab[0] = 11;
	cout << tab[0] << endl;
	
	
}
