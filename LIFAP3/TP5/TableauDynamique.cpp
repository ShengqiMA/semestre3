#include <iostream>
#include "TableauDynamique.h"
#include "ElementTD.h"
using namespace std;
TableauDynamique::TableauDynamique(){
	ad = new ElementTD[1];
	capacite = 1;
	taille_utilisee = 0;
}

TableauDynamique::TableauDynamique(const TableauDynamique & t){
	capacite = t.capacite;
	taille_utilisee = t.taille_utilisee;
	ad = new ElementTD[capacite];
	for(int i = 0;i < (int)taille_utilisee;i++){
		ad[i] = t.ad[i];
	}
}

TableauDynamique::~TableauDynamique(){
	cout << "TableauDynamique destruire" << endl;
	delete [] ad;
	ad = NULL;
	capacite = 0;
	taille_utilisee = 0;
}

void TableauDynamique::vider(){
	delete [] ad;
	taille_utilisee = 0;
	capacite = 0;
}

void TableauDynamique::ajouterElement(ElementTD e){
	if(capacite > taille_utilisee){
		ad[taille_utilisee] = e;
		taille_utilisee++;
	}else{
		ElementTD * adcopie = new ElementTD[capacite*2];
		for(int i = 0;i < (int)capacite;i++){
			adcopie[i] = ad[i];
		}
		adcopie[capacite] = e;
		ad = adcopie;
		adcopie = NULL;
		capacite *= 2;
		taille_utilisee++;
	}
}

ElementTD TableauDynamique::valeurIemeElement(unsigned int indice) const{
	if(indice < taille_utilisee && indice >= 0)
	return ad[indice-1];
}

void TableauDynamique::modifierValeurIemeElement(ElementTD e, unsigned int indice){
	ad[indice] = e;
}

void TableauDynamique::afficher() const{
	for(int i = 0;i < (int)taille_utilisee;i++){
		afficheElementTD(ad[i]);
		std::cout << "  ";
	}
	std::cout << "\n";
}

void TableauDynamique::supprimerElement(unsigned int indice){
	if(taille_utilisee != 0 && indice >= 0 && indice < capacite){
		for(int i = indice;i < (int)taille_utilisee-1;i++){
			ad[i] = ad[i+1];
		}
		ad[taille_utilisee-1] = 0;
		taille_utilisee--;
	}
	if(taille_utilisee < (capacite /3)){
		ElementTD * adcopie = new ElementTD[capacite/2];
		for(int i = 0;i < (int)taille_utilisee;i++){
			adcopie[i] = ad[i];
		}
		ad = adcopie;
		adcopie = NULL;
		capacite /= 2;
	}
}

void TableauDynamique::insererElement(ElementTD e, unsigned int indice){
	if(indice >= 0 && indice < taille_utilisee && capacite > taille_utilisee){
		for(unsigned int i = taille_utilisee;i > indice;i--){
			ad[i] = ad[i-1];
		}
		ad[indice] = e;
		taille_utilisee++;
	}
}

void TableauDynamique::trier(){
	unsigned int min;
	ElementTD t;
	for(unsigned int i = 0;i < taille_utilisee-1;i++){	
		min = i;	
		for(unsigned int j = i+1;j < taille_utilisee;j++){
			if(ad[j] < ad[min]){
				min = j;
			}
		}
		if(min != i){
			t = ad[min];
			ad[min] = ad[i];
			ad[i] = t;
		}
	}
}

int TableauDynamique::rechercherElement(ElementTD e) const{
	for(int i = 0;i < (int)taille_utilisee;i++){
		if(ad[i] == e)
			return i;
	}
	return -1;
}

