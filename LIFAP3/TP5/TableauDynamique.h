#ifndef _TAB_DYN
#define _TAB_DYN

#include "ElementTD.h"

class TableauDynamique {
public:
    /* donn閟 membres */
    /* =============== */
    /* (ne sont pas cens閑s 阾re connues par l'utilisateur,
        si on avait pu ne pas les faire figurer dans l'interface, on l'aurait fait)
    */
    unsigned int capacite;
    unsigned int taille_utilisee;
    ElementTD * ad;

    /* fonctions membres */
    /* ================= */
    TableauDynamique (); // Constructeur par d閒aut
    /* Postcondition : le tableau a 1 emplacement allou?mais vide (taille utilis閑 nulle) */

    TableauDynamique (const TableauDynamique& t); // Constructeur par copie
    /* Precondition : le tableau t est initialis?*/
    /* Postcondition : les deux tableaux contiennent des s閝uences d'ElementTD identiques (copie de t) */

    ~TableauDynamique (); // Destructeur
    /* Postcondition : la m閙oire allou閑 dynamiquement est lib閞閑. */
	
	void vider ();
    /* Postcondition : la m閙oire allou閑 dynamiquement est lib閞閑. 
	                   le tableau a 1 emplacement allou?mais vide (taille utilis閑 nulle) */

    void ajouterElement (ElementTD e);
    /* Postcondition : l'閘閙ent e est ajout?dans le premier emplacement inutilis?du tableau,
                       la taille est incr閙ent閑 de 1. Doublement de la capacit?si n閏essaire. */

    ElementTD valeurIemeElement (unsigned int indice) const;
    /* Precondition : 0 <= indice < taille_utilisee */
    /* Resultat : retourne l'ElementTD ?l'indice en param鑤re */

    void modifierValeurIemeElement (ElementTD e, unsigned int indice);
    /* Precondition : 0 <= indice < taille_utilisee */
    /* Postcondition : l'ElementTD ?l'indice en param鑤re vaut e */

    void afficher () const;
    /* Postcondition : les 閘閙ents du tableau sont affich閟 sur la sortie standard (espacement entre les 閘閙ents)
                       en utilisant la proc閐ure d'affichage de ElementTD */

    void supprimerElement (unsigned int indice);
    /* Precondition : le tableau est non vide, et 0 <= indice < taille_utilisee */
    /* Postcondition : la taille utilis閑 du tableau est d閏r閙ent閑 de 1.
                       si taille_utilisee < capacite/3, alors on divise la capacit?par 2. */

    void insererElement (ElementTD e, unsigned int indice);
    /* Precondition : 0 <= indice < taille_utilisee */
    /* Postcondition : e est ins閞??l'indice en param鑤re et la taille utilis閑 est incr閙ent閑 de 1 */

    void trier ();
    /* Postcondition : le tableau est tri?dans l'ordre croissant des valeurs des 閘閙ents */

    int rechercherElement (ElementTD e) const;
    /* Precondition : le tableau est tri?dans l'ordre croissant */
    /* R閟ultat : indice de l'emplacement qui contient un ElementTD 間al ?e,
                  -1 si le tableau ne contient pas d'閘閙ent 間al ?e */

};

#endif
