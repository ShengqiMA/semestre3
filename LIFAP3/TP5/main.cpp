#include <iostream>
#include "TableauDynamique.h"
#include "ElementTD.h"
using namespace std;
int main(){
	TableauDynamique tb;
  	for(int i = 5;i >= 0 ;i--){
    		tb.ajouterElement(i);
  	}
  	tb.afficher();
  	
  	TableauDynamique tb1(tb);
  	tb.vider();
  	tb.afficher();
  	
  	cout << "tb1:" << endl;
  	
  	tb1.afficher();
  	cout << "iemeElement: 4 :" << tb1.valeurIemeElement(4) << endl;
  	
  	tb1.modifierValeurIemeElement(9,1);
  	tb1.afficher();
  	
  	tb1.supprimerElement(0);
  	tb1.afficher();
  	
 	tb1.insererElement(3,0);
 	tb1.afficher();
 	
 	tb1.trier();
 	tb1.afficher();
 	
 	cout << tb1.rechercherElement(3) << endl;

  	return 0;
}
