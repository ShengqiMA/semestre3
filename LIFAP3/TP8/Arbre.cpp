#include <iostream>
#include "Arbre.h"
#include "ElementA.h"
using namespace std;

Arbre::Arbre(){
	adRacine = NULL;
}

Arbre::~Arbre(){
	supprimerNoeud(adRacine);
}

void Arbre::vider(){
	supprimerNoeud(adRacine);
}

bool Arbre::estVide() const{
	return adRacine == NULL;
}

void Arbre::insererElement(ElementA e){
	Noeud * actu = adRacine;
	if(actu == NULL){
		actu = new Noeud;
		actu->info = e;
		actu->fd = NULL;
		actu->fg = NULL;
		adRacine = actu;
	}
	while(actu != NULL){
		if(actu->info < e){
			if(actu->fd != NULL){
				actu = actu->fd;
			}else{
				actu->fd = new Noeud;
				actu = actu->fd;
				actu->info = e;
				actu->fd = NULL;
				actu->fg = NULL;
			}
			
		}else if(actu->info > e){
			if(actu->fg != NULL){
				actu = actu->fg;
			}else{
				actu->fg = new Noeud;
				actu = actu->fg;
				actu->info = e;
				actu->fd = NULL;
				actu->fg = NULL;
			}
		}else{
			break;
		}
	}
}

void Arbre::afficherParcoursInfixe() const{
	afficher(adRacine);
}

void Arbre::rechercherElement (ElementA e, bool & trouve, int & nb_visites) const{
	Noeud * actu = adRacine;
	nb_visites = 0;
	trouve = false;
	while(actu != NULL){
		nb_visites++;
		if(e < actu->info){
			actu = actu->fg;
		}else if(e > actu->info){
			actu = actu->fd;
		}else{
			trouve = true;
			break;
		}
	}
}

int Arbre::hauteurArbre() const{
	Noeud * actu = adRacine;
	if(estVide()) return -1;
	return countHauteur(actu);
}

int Arbre::countHauteur(Noeud * actu) const{
	int nbG = 0;
	int nbD = 0;
	if(actu->fg != NULL){
		nbG += countHauteur(actu->fg) + 1;
	}else if(actu->fd != NULL){
		nbD += countHauteur(actu->fd) + 1;
	}
	if(nbG > nbD){
		return nbG;
	}else{
		return nbD;
	}
}
void Arbre::afficher(Noeud * actu) const{
	if(actu->fg == NULL){
		afficherElementA(actu->info);
		cout << " ";
	}else{
		afficher(actu->fg);
		afficherElementA(actu->info);
		cout << " ";
		if(actu->fd != NULL)
			afficher(actu->fd);
	}
}

void Arbre::supprimerNoeud(Noeud * actu){
	if(actu != NULL){
		supprimerNoeud(actu->fg);
		supprimerNoeud(actu->fd);	
		delete actu;
		actu = NULL;
	}
}
