#include <iostream>
#include "Arbre.h"
#include "ElementA.h"
using namespace std;

int main(){
	Arbre a;
	a.insererElement(5);
	a.insererElement(3);
	a.insererElement(1);
	a.insererElement(4);
	a.afficherParcoursInfixe();
	
	int t = 4;
	bool trouve;
	int nb_visites;
	a.rechercherElement(t,trouve, nb_visites);
	if(trouve) cout << "\n" << "trouver" << t << " avec nombre de visites: "<< nb_visites << endl;
	
	cout << "hauteur: " << a.hauteurArbre() << endl;
	return 0; 
}
