#include<iostream>
#include<fstream>
#include<string>
#include<math.h>
#include<cstdlib>
#include<time.h>

using namespace std;

class NbComplexe{
	public:
		double re;
		double im;
		//在结构体或者类中 
		//当对象或者参数为地址值时 不能使用 '.' time.day而使用 '->' time->day
		//C++ 中 this为地址值  
		NbComplexe() : re(0), im(0) {
		}
		//构造函数初始化列表以一个冒号开始，接着是以逗号分隔的数据成员列表，每个数据成员后面跟一个放在括号中的初始化式
		NbComplexe(double a, double b) : re(a), im(b) {
			//this->re = re;
			//this->im = im;
		}
		NbComplexe(const NbComplexe & nbc){
			re = nbc.re;
			im = nbc.im;
		}
		~NbComplexe(){
			cout << "NbComplexe detruire" << endl;
		}
		
		void saisir(){
			cout << "reel:";
			cin >> re;
			cout << "imaginaire:";
			cin >> im;
		} 
		void afficher() const{
			if(im >= 0.0){
				cout << re << '+' << im << 'i' << endl;
			}else{
				cout << re << im << 'i' << endl;
			}
		}
		void multiplier(const NbComplexe & nbc){
			double ret, imt;
			ret = nbc.re * re - nbc.im * im;
			imt = im * nbc.re + re * nbc.im;
			re = ret;
			im = imt;
		}
		double module() const{
			return sqrt(re * re + im * im);
		}
		bool estPlusPetit(const NbComplexe & c) const{
			return module() < c.module();
		}
		
		bool operator = (const NbComplexe & c) {
			re = c.re;
			im = c.im;
		}
		 
		bool operator < (const NbComplexe & c) const{
			return module() < c.module();
		}
		
		NbComplexe operator * (const NbComplexe & c){
			NbComplexe r;
			double ret, imt;
			ret = c.re * re - c.im * im;
			imt = im * c.re + re * c.im;
			r.re = ret;
			r.im = imt;
			return r;
		}
		
		friend ostream & operator << (ostream & os, NbComplexe &c){
			if(c.im >= 0.0){
				cout << c.re << '+' << c.im << 'i' << endl;
			}else{
				cout << c.re << c.im << 'i' << endl;
			} 
		}
		
		friend istream & operator >> (istream & is, NbComplexe &c){
			cout << "reel:";
			cin >> c.re;
			cout << "imaginaire:";
			cin >> c.im;
		}
	

		
};

void trierParSelection(NbComplexe * tab, int taille){
	NbComplexe t;
	for(int i = 0;i < taille - 1;i++){
		int indice = i;
		for(int j = i+1;j < taille;j++){
			if(tab[j] < tab[indice]){
				indice = j;
			}
		}
		t = tab[i];
		tab[i] = tab[indice];
		tab[indice] = t;
	}
}

void trierParInsertion(NbComplexe * tab, int taille){
	NbComplexe t;
	for(int i = 1;i < taille;i++){
		int j = i - 1;
		t = tab[i];
		while(j >= 0 && t < tab[j]){
			tab[j+1] = tab[j];
			j--;
		}
		tab[j+1] = t;
	}
}

void lireTabNbComplexeDepuisFichier(NbComplexe[] tab, int taille, string nom_fichier){
	ifstream infile;
	if(infile.open(nom_fichier)){
		infile >> taille;
		for(int i = 0;i < taille;i++){
			infile >> tab[i].re >> tab[i].im;
		}			
	}
	infile.close();
}

int main(){
	
	NbComplexe nbc;
	nbc.saisir();
	nbc.afficher();
	cout << sizeof(nbc) << endl;
	
	NbComplexe nbc1(nbc);
	nbc1.afficher();
	NbComplexe * nbtas = new NbComplexe(3,4);
	nbtas->afficher();
	nbc1.multiplier(*nbtas);
	nbc1.afficher();
	delete nbtas;
	//delete [] nbtas; 当对象所在的函数已调用完毕 系统自动执行析构函数  
	nbtas = NULL;
	
	//exo 3
	NbComplexe nbc2(2,3);
	NbComplexe nbc3(3,4);
	cout << "module2:" << nbc2.module() << "module3:" << nbc3.module() << endl;
	
	//exo4
	int taille;
	cout << "saisir taille de tableau: ";
	cin >> taille;
	NbComplexe * tab = new NbComplexe[taille];
	srand((unsigned)time(NULL));
	for(int i = 0;i < taille;i++){
		tab[i].re = (double)((rand() % 201) - 100) / 10;
		tab[i].im = (double)((rand() % 201) - 100) / 10;
		cout << "tab[" << i << "]= ";
 		cout << "(mod= " << tab[i].module() << ") ";
 		tab[i].afficher();
	}
	trierParSelection(tab, taille);
	for(int i = 0;i < taille;i++){
		cout << "tab[" << i << "]= ";
 		cout << "(mod= " << tab[i].module() << ") ";
		tab[i].afficher();
	}
	trierParInsertion(tab, taille);
	for(int i = 0;i < taille;i++){
		cout << "tab[" << i << "]= ";
 		cout << "(mod= " << tab[i].module() << ") ";
		tab[i].afficher();
	}

	delete [] tab;
	cout << "-------------------" << endl;
	
	NbComplexe c1;
	c1.afficher();
	NbComplexe c2(2,3);
	c2.afficher();
	
	if(c1 < c2) cout << "oui" << endl;
	else cout << "non" << endl;
	c1 = c2;
	c1.afficher();
	c2.afficher();
	
	(c1 * c2).afficher();
	
	cout << c1 << endl;
	return 0;
}
