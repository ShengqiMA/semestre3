#include<iostream>
using namespace std;
class Date{
	public:
		int jour, mois, annee;
		int * siecle;
		//无参构造 
		Date() {
			jour = 1;
			mois = 1;
			annee= 1990;
		}
		//带参构造 
		Date(int j, int m, int a ) {
			jour = j;
			mois = m;
			annee= a;
		}
		Date(const Date & d) {
			jour = d.jour;
			mois = d.mois;
			annee = d.annee;
			siecle = new int(*(d.siecle));
		}
		
		//this 是指针类型 
		Date* ajouteJours(int jour) {
			this -> jour += jour; // jour += jour aurait modifié le paramètre jour
			return this; // retourne un pointeur sur l’objet
		}
		//函数的参数可以初始化值
		//初始化的参数可以是所有参数，也可以是后几个参数，但不能只初始化前几个参数 
			//void incrementer(intnbJour= 1, intnbMois= 1, intnbAnnee) { ... }	报错 
		//初始化的参数也可以在调用时赋值，新值将覆盖初始化的值 
			//uneDate.incrementer(2,5,3); // OK, utilise (2,5,3) n'est pas  2 5 1
		void incrementer(int nbJour= 1, int nbMois= 1, int nbAnnee= 1) {
			jour += nbJour; mois += nbMois; annee += nbAnnee;
			// + vérifications: j < 28/29/30/31 et m < 12
		}
		//析构函数 
		~Date() {
			cout << "Date détruite" << endl;
		}
};
//如果声明了有参数构造函数，则编译器不会自动添加默认构造函数 
int main(){
	Date uneDate;
	Date autreDate(2,2,2000);
	Date copy = uneDate;
	cout << "adresse de uneDate: " << &uneDate << " adresse de copy: " << &copy << endl;
	//两个对象的地址不相同，也就是只复制了两个对象的值 
	cout << "adresse de uneDate.jour : " << &uneDate.jour << " adresse de copy.jour : " << &copy.jour << endl;
	cout << "adresse de uneDate.mois : " << &uneDate.mois << " adresse de copy.mois : " << &copy.mois << endl;
	cout << "adresse de uneDate.annee : " << &uneDate.annee << " adresse de copy.annee : " << &copy.annee << endl;
	return 0;
}
