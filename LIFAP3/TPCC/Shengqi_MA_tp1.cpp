//Shengqi MA p2108131
#include <iostream>
using namespace std;

struct NombreReel{
	bool type;
	float simlep;
	double doublep;
};

void saisirNombreReel(NombreReel* nb){
	int t;
	
	cout << "1 pour double else pour float" << endl;
	cin >> t;
	if(t == 1){
		cout << "double :";
		nb -> type = false;
		cin >> nb->doublep;
	}else{
		cout << "float :";
		nb -> type = true;
		cin >> nb->simlep;
	}
}

void afficherNombreReel(const NombreReel* nb){
	if(nb->type){
		cout << nb->simlep << "(float)" << endl; 
	}else{
		cout << nb->doublep << "(double)" << endl; 
	}
	
}

void convertirNombreReel(NombreReel* nb){
	if(nb->type){
		nb->type = false;
		nb->doublep = (double)nb->simlep;
	}else{
		nb->type = true;
		nb->simlep = (float)nb->doublep;
	}
}


int main(){
	NombreReel nombre;
	saisirNombreReel(&nombre);
	afficherNombreReel(&nombre);
	 
	convertirNombreReel(&nombre);
	afficherNombreReel(&nombre);
	double d = 0.13131313131313;
	cout << d << endl;
	cout << float(d) << endl;
}
