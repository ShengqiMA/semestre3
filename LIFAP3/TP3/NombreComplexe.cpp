#include<iostream>
#include<string>
#include<math.h>
#include<cstdlib>
#include<time.h>

using namespace std;


class NbComplexe{
	public:
		double re;
		double im;
		//在结构体或者类中 
		//当对象或者参数为地址值时 不能使用 '.' time.day而使用 '->' time->day
		//C++ 中 this为地址值  
		NbComplexe() : re(0), im(0) {
		}
		//构造函数初始化列表以一个冒号开始，接着是以逗号分隔的数据成员列表，每个数据成员后面跟一个放在括号中的初始化式
		NbComplexe(double a, double b) : re(a), im(b) {
			//this->re = re;
			//this->im = im;
		}
		NbComplexe(const NbComplexe & nbc){
			re = nbc.re;
			im = nbc.im;
		}
		~NbComplexe(){
			cout << "NbComplexe detruire" << endl;
		}
		
		void saisir(){
			cout << "reel:";
			cin >> re;
			cout << "imaginaire:";
			cin >> im;
		} 
		void afficher() const{
			if(im >= 0.0){
				cout << re << '+' << im << 'i' << endl;
			}else{
				cout << re << im << 'i' << endl;
			}
		}
		void multiplier(const NbComplexe & nbc){
			double ret, imt;
			ret = nbc.re * re - nbc.im * im;
			imt = im * nbc.re + re * nbc.im;
			re = ret;
			im = imt;
		}
		double module() const{
			return sqrt(re * re + im * im);
		}
		bool estPlusPetit(const NbComplexe & c) const{
			return module() < c.module();
		}
};

void trierParSelection(NbComplexe * tab, int taille){
	NbComplexe t;
	for(int i = 0;i < taille - 1;i++){
		int indice = i;
		for(int j = i+1;j < taille;j++){
			if(tab[j].estPlusPetit(tab[indice])){
				indice = j;
			}
		}
		t = tab[i];
		tab[i] = tab[indice];
		tab[indice] = t;
	}
}

void trierParInsertion(NbComplexe * tab, int taille){
	NbComplexe t;
	for(int i = 1;i < taille;i++){
		int j = i - 1;
		t = tab[i];
		while(j >= 0 && t.estPlusPetit(tab[j])){
			tab[j+1] = tab[j];
			j--;
		}
		tab[j+1] = t;
	}
}

int main(){
	NbComplexe nbc;
	nbc.saisir();
	nbc.afficher();
	cout << sizeof(nbc) << endl;
	
	NbComplexe nbc1(nbc);
	nbc1.afficher();
	NbComplexe * nbtas = new NbComplexe(3,4);
	nbtas->afficher();
	nbc1.multiplier(*nbtas);
	nbc1.afficher();
	delete nbtas;
	//delete [] nbtas; 当对象所在的函数已调用完毕 系统自动执行析构函数  
	nbtas = NULL;
	
	//exo 3
	NbComplexe nbc2(2,3);
	NbComplexe nbc3(3,4);
	cout << "module2:" << nbc2.module() << "module3:" << nbc3.module() << endl;
	
	//exo4
	int taille;
	cout << "saisir taille de tableau: ";
	cin >> taille;
	NbComplexe * tab = new NbComplexe[taille];
	srand((unsigned)time(NULL));
	for(int i = 0;i < taille;i++){
		tab[i].re = (double)((rand() % 201) - 100) / 10;
		tab[i].im = (double)((rand() % 201) - 100) / 10;
		cout << "tab[" << i << "]= ";
 		cout << "(mod= " << tab[i].module() << ") ";
 		tab[i].afficher();
	}
	trierParSelection(tab, taille);
	for(int i = 0;i < taille;i++){
		cout << "tab[" << i << "]= ";
 		cout << "(mod= " << tab[i].module() << ") ";
		tab[i].afficher();
	}
	trierParInsertion(tab, taille);
	for(int i = 0;i < taille;i++){
		cout << "tab[" << i << "]= ";
 		cout << "(mod= " << tab[i].module() << ") ";
		tab[i].afficher();
	}

	delete [] tab;
	return 0;
}
