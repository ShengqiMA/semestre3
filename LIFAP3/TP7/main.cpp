#include <iostream>
using namespace std;

#include "File.h"
#include "ElementL.h"
#include "Liste.h"

int main(){
	File f;
	
	int a = 3;
	ElementL ae = &a;
	f.enfiler(ae);
	cout << *((int *)f.premierDeLaFile()) << endl;
	return 0;
}
