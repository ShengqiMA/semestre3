#include <iostream>
#include "File.h"

File::File(){
}

File::~File(){
	l.vider();
}

File & File::operator = (const File & f){
	l=f.l;
	return *this;
}

unsigned int File::nbElements() const{
	return l.nbElements();
}

void File::defiler(){
	l.supprimerTete();
}

void File::enfiler(ElementF e){
	l.ajouterEnQueue(e);
}

ElementF File::premierDeLaFile() const{
	if(!estVide()){
		return l.iemeElement(0);
	}
	return NULL;
}

bool File::estVide() const{
	return l.estVide();
}

