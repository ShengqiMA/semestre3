#include<iostream>
#include<string>
using namespace std;

int somme(int a, int b){
	return a + b;
}
void PrecEtSuiv (int x, int & prec, int & suiv) {
	prec = x-1;
	suiv = x+1;
}

struct student{
	int age;
	string nom;
};

int main(){
	cout << "Taille de int" << sizeof(short) << endl;
	cout << "Taille de int" << sizeof(int) << endl;
	cout << "Taille de char" << sizeof(char) << endl;
	cout << "Taille de long" << sizeof(long) << endl;
	cout << "Taille de float" << sizeof(float) << endl;
	cout << "Taille de double" << sizeof(double) << endl;
	
	cout << somme(1, 5) << endl;
	
	int a = 5;
	int *p = &a;
	
	cout << a << endl;
	cout << p << endl;
	cout << *p << endl;
	cout << &a << endl;
	
	int x = 100; int y = 15; int z = 8;
	PrecEtSuiv(x,y,z);
	cout << "Pr��c��dent=" << y << ", Suivant= " << z;
	
	string question("hello world");
	cout << '\n' << question << endl;
	char tab[] = "Bonjour";
	cout << tab << endl;
	cout << question.size() << endl;
	cout << question.find('d') << endl;
	
	student std1;
	std1.age = 11;
	std1.nom = "Tom";
	cout << std1.age << '\n' << std1.nom << endl;
	return 0;
}


