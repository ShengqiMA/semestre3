//防止重复引用CM7_h文件 
#ifndef POINT_H //if not define CM7.h 如果没有定义 
#define POINT_H //deifine CM7.h	则定义

#include<string>

class Point{
	public:
		int x,y;
		
		Point();
		Point(int x, int y);
		~Point();
		
		void deplacerX(int n);
		void afficher() const; //const代表该成员函数无法修改成员变量  const函数只能调用const函数 
	
	private:
		void changerPosition(int x,int y);
		//所有的成员变量 构造函数 析构函数 成员函数 不得被隐藏 
};
#endif 
 
