#include "Point.h"
#include <iostream>

//Point:: 指明该函数实现Point类的成员 
Point::Point(){
	//可以直接访问成员变量 
	x = 10;
	y = 10;
}
Point::Point(int x,int y){
	this->x = x;
	this->y = y;
}

Point::~Point(){
	std::cout << "Point detruire" << std::endl;
}

void Point::deplacerX(int n){
	this->x += n;
}

void Point::afficher() const{
	std::cout << "x: " << x << "y: " << y << std::endl; 
}
void Point::changerPosition(int x, int y){
	this->x = x;
	this->y = y;
}

int main(){
	Point point;
	point.afficher();
	return 0;
}

