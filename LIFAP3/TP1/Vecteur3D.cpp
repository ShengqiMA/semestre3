#include<iostream>
#include<math.h>
#include<stdlib.h>
#include<time.h>
using namespace std;

struct Vecteur3D{
	float x,y,z;
};

float Vecteur3DGetNorme(const Vecteur3D& v) {
 	return sqrt( pow( v.x,2) + pow(v.y,2) + pow(v.z,2) );
}

void Vecteur3DNormaliser(Vecteur3D& v){
	float longeur = Vecteur3DGetNorme(v);
	v.x /= longeur;
	v.y /= longeur;
	v.z /= longeur;
}

bool Vecteur3DEstNormalise(const Vecteur3D& v){
	return abs( pow(v.x,2) + pow(v.y,2) + pow(v.z,2)) == 1;
}

Vecteur3D Vecteur3DAdd(const Vecteur3D& v1, const Vecteur3D& v2){
	Vecteur3D somme;
	somme.x = v1.x + v2.x;
	somme.y = v1.y + v2.y;
	somme.z = v1.z + v2.z;
	return somme;
}

void Vecteur3DAfficher(const Vecteur3D v){
	cout << '(' << v.x << ", " << v.y << ", " << v.z << ')';
}
void Vecteur3DRemplirTabVecteurs(Vecteur3D tab[], int taille){
	srand((unsigned)time(NULL));
	for(int i = 0;i < taille;i++){
		Vecteur3D v = {rand() * (20.0 / RAND_MAX) - 10.0, rand() * (20.0 / RAND_MAX) - 10.0, rand() * (20.0 / RAND_MAX) - 10.0};
		//rand()返回0 - randmax之间随机数 则获得0-1之间的随机数为 rand() / RAND_MAX
		tab[i] = v;
	}
	cout << "tab[1].x = " << tab[1].x << "tab[1].y = " << tab[1].y << "tab[1].z = " << tab[1].z << endl;
	cout << "tab[taille-1].x = " << tab[taille-1].x << "tab[taille-1].y = " << tab[taille-1].y << "tab[taille-1].z = " << tab[taille-1].z << endl;
}
void Vecteur3DAfficherTabVecteurs(Vecteur3D tab[], int taille){
	for(int i = 0;i < taille;i++){
		cout << "vec" << i + 1 << " : ";
		Vecteur3DAfficher(tab[i]);
		if (i!=taille-1) cout << " ; " << endl;
	}
}

int Vecteur3DMaxTabVecteurs(Vecteur3D tab[], int taille){
	int indice = 0;
	for(int i = 1;i < taille;i++){
		if(Vecteur3DGetNorme(tab[indice]) < Vecteur3DGetNorme(tab[i])){
			indice = i;
		}	
	}
	return indice;
}

void Vecteur3DConcatenationTabVecteurs(const Vecteur3D tab1[], const Vecteur3D tab2[], Vecteur3D tab3[], int taille1, int taille2){
	int i;
	for(i = 0;i < taille1;i++){
		tab3[i] = tab1[i];
	}
	int j;
	for(j = 0;j < taille2;j++){
		tab3[i+j] = tab2[j];
	}
}

void Vecteur3DInverseTabVecteurs(Vecteur3D tab[], int taille){
	Vecteur3D m;
	for(int i = 0;i < taille/2;i++){
		m = tab[i];
		tab[i] = tab[taille-1-i];
		tab[taille-1-i] = m;
	}
}

int main () {
 Vecteur3D vecteur1 = {5,2,1};
 Vecteur3D vecteur2 = {0,3,2};
 cout << "vecteur1 non normalise: ";
 Vecteur3DAfficher(vecteur1);
 cout << endl;
 cout << "vecteur2 non normalise: ";
 Vecteur3DAfficher(vecteur2);
 cout << endl;
 cout << "somme: ";
 Vecteur3DAfficher(Vecteur3DAdd(vecteur1,vecteur2));
 cout << endl;
 Vecteur3DNormaliser(vecteur1);
 Vecteur3DNormaliser(vecteur2);
 cout << "vecteur1 normalise: ";
 Vecteur3DAfficher(vecteur1);
 cout << endl;
 cout << "vecteur2 normalise: ";
 Vecteur3DAfficher(vecteur2);
 cout << endl;
 cout << "somme: ";
 Vecteur3D somme = Vecteur3DAdd(vecteur1,vecteur2);
 Vecteur3DAfficher(somme);
 if (Vecteur3DEstNormalise(somme)) cout << " est normalise" << endl;
 else cout << " n'est pas normalise" << endl;
 
 if (Vecteur3DEstNormalise(vecteur1)) cout << " est normalise" << endl;
 else cout << " n'est pas normalise" << endl;
 
 if (Vecteur3DEstNormalise(vecteur2)) cout << " est normalise" << endl;
 else cout << " n'est pas normalise" << endl;
 //exo5
 Vecteur3D tab[30];
 cout << "adresse de tab : " << (long long)&tab << endl;
 cout << "adresse de tab[0] : " << (long long)&tab[0] << endl;
 cout << "adresse de tab[1] : " << (long long)&tab[1] << endl;
 Vecteur3DRemplirTabVecteurs(tab, 30);
 Vecteur3DAfficherTabVecteurs(tab, 30);
 //exo6
 //a
 Vecteur3D tab1[5];
 Vecteur3DRemplirTabVecteurs(tab1, 5);
 Vecteur3DAfficherTabVecteurs(tab1, 5);
 Vecteur3D tab2[6];
 Vecteur3DRemplirTabVecteurs(tab2, 6);
 Vecteur3DAfficherTabVecteurs(tab2, 6);
 Vecteur3D tab3[11];
 Vecteur3DConcatenationTabVecteurs(tab1, tab2, tab3, 5, 6);
 Vecteur3DAfficherTabVecteurs(tab3, 11);
 //b
 Vecteur3DInverseTabVecteurs(tab1, 5);
 Vecteur3DAfficherTabVecteurs(tab1, 5);
 
 return 0;
}

