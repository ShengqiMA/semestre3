			.ORIG x3000
			LEA R6,stackend ; initialisation du pointeur de pile
			JSR saisie
			
			HALT
			
saisie:		ADD R6,R6,#-1
			STR R7,R6,#0
			
			LEA R3,caractere
			LEA R0,msg
			PUTS
			
loop:		GETC
			OUT
			ADD R1,R0,#-10
			STR R0,R3,#0
			ADD R3,R3,#1
			BRz endloop
			BR loop
endloop:	AND R1,R1,#0
			ST R1,caractere
			
			LDR R7,R6,#0
			ADD R6,R6,#1
			RET
msg:		.STRINGZ "en:"
caractere:	.BLKW #20
			; Pile
stack: 		.BLKW #32
stackend: 	.FILL #0
			.END
